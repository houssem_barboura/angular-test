import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LandingPageComponent } from './feature/test/components/landing-page/landing-page.component';
import { ProgressBarComponent } from './feature/test/components/progress-bar/progress-bar.component';

@NgModule({
  declarations: [
    AppComponent,
    LandingPageComponent,
    ProgressBarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
