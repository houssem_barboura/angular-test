import { Component } from '@angular/core';

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.scss'],
})
export class LandingPageComponent {
  ListOfProgressBar = [
    {
      name: 'Initialisation du test technique',
      value: 50,
    },
    {
      name: 'Avancement de la phase de dévloppement',
      value: 25,
    },
  ];
  changeValue(condition: string) {
    if (condition == 'zero') {
      this.ListOfProgressBar.map((x: any) => {
        x.value = 0;
      });
    }
    if (condition == '5%') {
      this.ListOfProgressBar.map((x: any) => {
        x.value = x.value + 5;
      });
    }
    if (condition == '10%') {
      this.ListOfProgressBar.map((x: any) => {
        x.value = x.value + 10;
      });
    }
  }

  deleteProgress(index:any){
    this.ListOfProgressBar.splice(index,1)
  }
  chnageValueOfItem(index:number,value:number){
    this.ListOfProgressBar[index].value= value
  }
  addProgress(){
    let progress=   {
      name: 'New progress ',
      value: 100,
    }
    this.ListOfProgressBar.push(progress)
  }
}
